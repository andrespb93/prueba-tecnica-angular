import { Usuario } from './../../app/models/usuario';

export const LISTA_USUARIOS: Usuario[] = [
  { id: 1, nombreUsuario: 'Dr Nice', email: '', genero: 'Masculino', descripcion: 'adasdsadasdsadasdgrgrtgregreg', localizacion: '' },
  { id: 2, nombreUsuario: 'Narco', email: '', genero: 'Masculino', descripcion: 'adasdsadasdsadasdgrgrtgregreg', localizacion: '' },
  { id: 3, nombreUsuario: 'Bombasto', email: '', genero: 'Masculino', descripcion: 'adasdsadasdsadasdgrgrtgregreg', localizacion: '' },
  { id: 4, nombreUsuario: 'Celeritas', email: '', genero: 'Masculino', descripcion: 'adasdsadasdsadasdgrgrtgregreg', localizacion: '' },
  { id: 5, nombreUsuario: 'Magneta', email: '', genero: 'Femenino', descripcion: 'adasdsadasdsadasdgrgrtgregreg', localizacion: '' },
  { id: 6, nombreUsuario: 'RubberMan', email: '', genero: 'Masculino', descripcion: 'adasdsadasdsadasdgrgrtgregreg', localizacion: '' },
  { id: 7, nombreUsuario: 'Dynama', email: '', genero: 'Femenino', descripcion: 'adasdsadasdsadasdgrgrtgregreg', localizacion: '' },
  { id: 8, nombreUsuario: 'Dr IQ', email: '', genero: 'Masculino', descripcion: 'adasdsadasdsadasdgrgrtgregreg', localizacion: '' },
  { id: 9, nombreUsuario: 'Magma', email: '', genero: 'Masculino', descripcion: 'adasdsadasdsadasdgrgrtgregreg', localizacion: '' },
  { id: 10, nombreUsuario: 'Tornado', email: '', genero: 'Masculino', descripcion: 'adasdsadasdsadasdgrgrtgregreg', localizacion: '' }
];
