import { FileI } from './../../../models/file';
import { UsuarioService } from './../../../services/usuario.service';
import { Component, OnInit } from '@angular/core';
import { Usuario } from 'src/app/models/usuario';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';

@Component({
  selector: 'app-usuario',
  templateUrl: './usuario.component.html',
  styleUrls: ['./usuario.component.css']
})
export class UsuarioComponent implements OnInit {

  image: FileI;
  currentImage = 'https://picsum.photos/id/113/150/150';

  usuarioForm: FormGroup;
  lstUsuarios: Usuario[];
  submitted = false;
  emailPattern: any = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

  constructor(
    private usuarioService: UsuarioService,
    private formBuilder: FormBuilder,
  ) { }

  ngOnInit(): void {
    this.usuarioService.getUsuarios().
      then(usuarios => this.lstUsuarios = usuarios);

    this.usuarioForm = this.formBuilder.group({
      nombreUsuario: ['', Validators.required],
      email: new FormControl('', [Validators.required, Validators.email, Validators.pattern(this.emailPattern)]),
      genero: [''],
      descripcion: [''],
      photoURL: new FormControl(''),
    });
  }

  get nombreUsuario() { return this.usuarioForm.get('nombreUsuario'); }
  get email() { return this.usuarioForm.get('email'); }

  get f() { return this.usuarioForm.controls; }

  onSubmit() {
    this.submitted = true;
    if (this.usuarioForm.invalid) {
      console.log('', this.usuarioForm.value);
      return;
    } else {
      console.log('valido');
      alert('Usuario guardado !');
    }
  }

  handleImage(image: FileI): void {
    this.image = image;
    console.log(this.image);
  }

}
