import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Usuario } from '../models/usuario';

@Injectable({
  providedIn: 'root'
})
export class UsuarioService {

  constructor(private http: HttpClient) { }

  getUsuarios() {
    return this.http.get<any>('assets/data/usuarios.json')
      .toPromise()
      .then(res => <Usuario[]> res.data)
      .then(data => { return data; });
    }
}
