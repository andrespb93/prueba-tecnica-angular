export class Usuario {
  id: number;
  nombreUsuario: string;
  email: string;
  genero: string;
  localizacion: string;
  descripcion: string;
}
